pdfFormula.py script

creates pdf files from a body of a .tex document
using pdfFormulaFiles\preamble.tex preamble and pdfFormulaFiles\definitions.tex definitions.
It finds all .tex documents in the directory it is launch from and all its subdirectories
and produces corresponding .pdf file.

pdfFormula.py and pdfFormulaFiles folder should be places at the same directory.

To run an example just run from the pdfformula directory with no arguments:
pdfFormula.py

see help for some more options
pdfFormula.py -h

#add pdfFormula.py python3 script path to system path
export PATH=/home/lukacjo1/Documents/GitPrjs/pdfformula:$PATH
