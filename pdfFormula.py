#!/bin/python3
import os.path #check if file exists
import sys #sys.argv cmd arguments
import subprocess #to run pdflatex

def getAvailableName(directory, baseName, extension = ''):
    """function getAvailableName(directory, baseName, extension)
       returns available name in the current <directory>.
       If the <directory>/<baseName><extension> does exist,
       it tries <directory>/<baseName>idx.<extension>
       where idx is 1,2,...
       Inputs:
         <directory> -- string
         <baseName>  -- string
         <extension> -- string (can be empty if directory name
           should be composed)
       Outputs:
         <availableName> -- string, available name
    """
    fullName = os.path.join(directory, baseName + extension)
    if not os.path.exists(fullName):
        return fullName
    idx = 1
    while os.path.exists( os.path.join(directory, baseName + '_' + str(idx) + extension) ):
        idx += 1
    return os.path.join(directory, baseName + '_' + str(idx) + extension)

def composeFile(formulaFN, preambleFN, definitionsFN, outFN):
    """function composeFile(formulaFN, preambleFN, definitionsFN, outFN)
       composes output (.tex) file.
       Inputs:
         <formulaFN> -- filename of formula / equation
         <preambleFN> -- preamble to be used in output .tex file
         <definitionsFN> -- .tex defined commands file name
         <outFN> -- output (.tex) file name
       Outputs:
         <status> -- True (ok), False (an error occured)
    """
    #check if all files exists
    files = [formulaFN, preambleFN, definitionsFN]
    for fn in files:
        if not os.path.isfile(fn):
            print('ERROR: file "' + fn + '" does not exists.')
            return False

    #open input files for reading and output file for writing
    with open(formulaFN,'r') as fFormula,\
         open(preambleFN,'r') as fPreamble,\
         open(definitionsFN,'r') as fDefinitions,\
         open(outFN,'w') as fOut:
        fOut.write( fPreamble.read() )
        fOut.write( "\n" )
        fOut.write( fDefinitions.read() )
        fOut.write( "\n\n\\begin{document}\n\n" )
        fOut.write( fFormula.read() )
        fOut.write( "\n\n\\end{document}\n" )
    return True

def getFiles(rootDir, ommitDirs, extension):
    """function getTexFiles(rootDir)
    return .tex files for the given <rootDir> directory.
    Inputs:
        <rootDir> -- string, root directory
        <ommitDirs> -- list of strings, directory names to ommit from search
        <extension> -- string '.<extension>', selection filter
    Outputs:
        <outNames> -- full names of files with extension <extension> in a list
    """
    outNames = []
    for (root, dirs, files) in os.walk(rootDir, topdown=True):
        ommitCurrent = False
        #print(root)
        #print(dirs)
        #print(files)
        for forbiddenDir in ommitDirs:
            if forbiddenDir in root:
                ommitCurrent = True
                break
        #print('ommit dir: ' + str(ommitCurrent))
        if ommitCurrent:
            continue
        for file in files:
            if file.endswith(extension):
                outNames.append(os.path.join(root,file))
    return outNames

def convertTexFiles(inAllTexFiles, preambleFN, definitionsFN, removeAuxSource = True, showStatus = False, epsGenerate = False):
    """function convertTexFiles(inTexFiles)
    constructs auxiliary file from the given files
    in <inTexFiles>, compiles it with pdflatex and removes notimportant files
    Inputs:
        <inAllTexFiles> -- list of tex files to process. Fullnames should be used.
        <preambleFN> -- string, filename of preamble to insert to .tex file
        <definitionsFN> -- string, filename of definitions to insert to .tex file
        <removeAuxSource> -- boolean value (True is default). Remove auxiliary source .tex files.
        <showStatus> -- boolean value (False is default). Show status of compilation.
        <epsGenerate> -- boolean value. Whether an .eps file needs to be generated.
    Outputs:
        <status> -- boolean. True if all files were processed correctly, False 
          otherwise.
    """
    idxFile = 1
    auxFile_suffix = '_aux'
    # remove .tex files, that contain auxFile_suffix before .tex
    inTexFiles = [f for f in inAllTexFiles if not f.endswith( auxFile_suffix + '.tex' )]
    #print(inTexFiles)
    #print(okTexFiles)    
    #print(type(inTexFiles))
    #return
    
    NFiles = len(inTexFiles)
    for currFile in inTexFiles:
        #inform user
        print(' ' + str(idxFile) + ' of (' + str(NFiles) + ')')
        idxFile += 1
        
        baseName = os.path.basename(currFile)
        dirName = os.path.dirname(currFile)
        baseName_root, extension = os.path.splitext(baseName)
        
        baseName_root_aux = baseName_root + auxFile_suffix
#        outFN = getAvailableName(directory = dirName, baseName = baseName_root_aux, extension = '.tex')
        # overwrite _aux.tex file if it exists, do not create another auxiliary file
        outFN = os.path.join(dirName, baseName_root_aux + '.tex')
        #compose filename
        isOK = composeFile(currFile, preambleFN, definitionsFN, outFN)
        
        if not isOK:
            print('ERROR: could not compose file: ' + outFN)
            continue
        
        outFN_baseName = os.path.basename(outFN)
        outFN_baseName_root = os.path.splitext(outFN_baseName)[0]
        
        #compose pdflatex command
        suppressOutput = ''
        #if showStatus:
        #    suppressOutput = ''
        #elif os.name == 'nt':
        #    suppressOutput = ' >nul 2>&1'
        #else:
        #    suppressOutput = '  &> /dev/null'
        
        
        #pdflatexcmd = 'pdflatex -halt-on-error -interaction=nonstopmode #-output-directory=%s %s %s' % \
        #(dirName, outFN_baseName, suppressOutput)
        
        #print('cmd: ' + pdflatexcmd)
        
        
        #run pdflatex command
        if showStatus:
            stat = subprocess.run(['pdflatex', '-halt-on-error', '-interaction=nonstopmode', \
            '-output-directory=' + dirName, outFN_baseName, suppressOutput], stdout=subprocess.PIPE)
        else:
            stat = subprocess.run(['pdflatex', '-halt-on-error', '-interaction=nonstopmode', \
            '-output-directory=' + dirName, outFN_baseName, suppressOutput], stdout=subprocess.PIPE)
        #stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
        
        status = stat.returncode
        #print('status: '  + str(status))
        
        # if the .pdf was generate without errors, try to generate .eps
        if status == 0 and epsGenerate:
            stat = subprocess.run(['latex', '-halt-on-error', '-interaction=nonstopmode', \
            '-output-directory=' + dirName, outFN_baseName, suppressOutput], stdout=subprocess.PIPE)
            #convert .dvi to .eps
            dvi_fn = os.path.join(dirName, baseName_root_aux + '.dvi')
            eps_fn = os.path.join(dirName, baseName_root_aux + '.eps')
            #print(dvi_fn)
            #print(eps_fn)
            if stat.returncode == 0:
                stat = subprocess.run(['dvips', '-E', \
            '-o', eps_fn, dvi_fn], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        
        
        if status != 0:
            print('ERROR: could not process file: ' + outFN + '\nCheck the file.\n')
            searchStr = '! LaTeX Error:'
        
            str_stdOut = stat.stdout.decode('UTF8')
            startIdx = str_stdOut.find(searchStr)
            extractedOut = str_stdOut[startIdx: ]
            #print('stdout: \n' + str_stdOut)
            #print('extracted: \n' + extractedOut)
            #print('stderr: \n' + stat.stderr.decode('UTF8'))
            
            #print error message
            print(extractedOut)
        
        if removeAuxSource and status == 0:
            #removeExt = ['.aux', '.log', '.tex']
            removeExt = ['.aux', '.log', '.tex', '.dvi']
        else:
            removeExt = ['.aux', '.log']

        #remove useless files        
        for auxExt in removeExt:
            auxFile = os.path.join(dirName, outFN_baseName_root + auxExt)
            if os.path.exists( auxFile ):
                os.remove(auxFile)
         
        
            
        #move the .pdf
        auxFile = os.path.join(dirName, outFN_baseName_root + '.pdf')
        if os.path.exists( auxFile ):
            finalPdfName = os.path.join(dirName, baseName_root + '.pdf')
            os.replace(auxFile, finalPdfName)
        #move the .eps
        auxFile = os.path.join(dirName, outFN_baseName_root + '.eps')
        if os.path.exists( auxFile ):
            finalPdfName = os.path.join(dirName, baseName_root + '.eps')
            os.replace(auxFile, finalPdfName)
        
if __name__ == '__main__':
    dirpath = os.getcwd() #current directory (from which is the program called)
    
    #collect .tex names to be processed
    pdfFormulaFilesDir = 'pdfFormulaFiles'
    texNames = getFiles(dirpath, [pdfFormulaFilesDir,'.git'] , '.tex')
    
    #real script path
    realPath = os.path.realpath(__file__)
    
    #preamble and definitions to be copied to output .tex files
    realPath_dir = os.path.dirname(realPath)
    preambleFN = os.path.join(realPath_dir, pdfFormulaFilesDir, 'preamble.tex')
    definitionsFN = os.path.join(realPath_dir, pdfFormulaFilesDir, 'definitions.tex')
    
    #print('real path: ' + realPath)
    #print('argv[0]: ' + sys.argv[0])
    
    #print(texNames)
    if '-h' in sys.argv or '--h' in sys.argv or '--help' in sys.argv:
        print("pdfFormula.py [options]\n" + \
        "   Converts latex fomulas in the given directory and subdirectories\n" + \
        "   to pdf files. Root directory is the one you launch \n" + \
        "   pdfFormula.py from\n\nOptions:\n" + \
        "   -no-rm -- do not remove auxiliary source files\n" + \
        "   -show-pdflatex -- show pdflatex output\n" + \
        "   -eps -- generate also .eps file\n" + \
        "   -h, -hh, --help -- show this help\n"
        )
        sys.exit()
    
    if '-no-rm' in sys.argv:
        removeAuxSource = False
    else:
        removeAuxSource = True
    
    if '-show-pdflatex' in sys.argv:
        showStatus = True
    else:
        showStatus = False
    
    if '-eps' in sys.argv:
        epsGenerate = True
    else:
        epsGenerate = False      
    
    convertTexFiles(texNames, preambleFN, definitionsFN, removeAuxSource, showStatus, epsGenerate)
    
